# Just WorkShop


.footer: Created By Alex M. Schapelle, VAioLabs.io

---

# About the workshop itself ?

We'll learn several topics mainly focused on:

- What is task runners ?
- What is Just?
- How Just works ?
- Where can we use just ?


### Who is this workshop for ?

- Junior/senior developers who wish to automate their development tasks.
- Junior/senior Sysadmins/SysOps who wish to automate tasks
- Developers that use CI/CD and want to make them easier.
- Experienced developers/devops who wish to learn new skill.


---

# Workshop topics

- Intro
- Just setup
- Basic Features
- Just Examples:
  - Python build
  - C build

---

# About me
<img src="99_misc/.img/me.jpg" alt="me" style="float:right;width:180px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        - I took hardware course and got A+.
        - Cisco course taught me  tcp/ip.
        - RedHat course was outcome of Cisco.
        - Eventually I learned of LPIC1 and Shell scripting in proper manner.
        - Other skills I've learned alone.
        - I tried to maintain Debian packages, but dropped it eventually.
---

# About me (cont.)
- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - Js admirer
    - Golang fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

You can find me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

---

# History and `Make` Tool 

In software development, `Make` is a build automation tool that automatically builds executable programs and libraries from source code by reading files called `Makefiles` which specify how to derive the target program. Though integrated development environments and language-specific compiler features can also be used to manage a build process, Make remains widely used, especially in Unix and Unix-like operating systems. 

However, `Make` mainly was designed for low level languages such as C/C++, while with other languages it did not gain the popularity. With Java came Ant and Maven, with other scripting languages there was no need to use build automation, thus make was put aside, until the CI/CD tools was introduced in to era of `DevOps`.

Within CI and CD pipeline, automation was done with shell commands inside `tasks`, yet combination of these shell commands used to be complicated and non reusable and here is where we meet new idea: `task runners`. Of course `make` is still usable, yet something more flexible was required, thus 2 new tools have emerged:

- [Just](https://just.systems):  reason of this workshop
- [Taskfile](https://taskfile.dev): something I'd suggest you to read on by yourself

---

# `Just`

`just` is a command runner, a handy way to save and run project-specific commands. Commands, called `recipes`, are stored in a file called `justfile` with syntax inspired by `make` and `makefile`.
For example:

```make
alias b := build

host := `uname -a`

build:
    cc *.c -o main

test-all: build
    ./test --all

test TEST: build
    ./test --test {{TEST}}
```
---

# `Just` (cont.)


You can list the recipes  with `--list` or `-l`
```sh
$ just  -l
Available recipes:
    build     # build main -> comments are shown when listing as descriptions
    b         # alias for `build`
    test TEST # run specific test
    test-all  # test everything
```

You can then run them with `just RECIPE_name`

```sh
$ just build
    cc *.c -o main
$ ./main
```
---

# `Just` (cont.)

Unlike `make`, `just` provides long list of capabilities and features that, not only run project specific commands, yet also run CI/CD commands, in any arbitrary tool such as Jenkins, Gitlab-CI and so on.

In upcoming chapters I'll try to provide and give relevant examples

