from flask import Flask, jsonify


app = Flask(__name__)
app.config.from_object('config')

@app.route('/')
def index():
    return jsonify(success=True), 200


@app.route('/<name>')
def hello(name):
    return jsonify(message=f'Hello {name}', success=True),200
    



if __name__ in  "__main__":
    app.run(port=app.config['PORT'],debug=app.config['DEBUG'],host=app.config['HOST'])