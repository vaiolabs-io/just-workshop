
---

# Basic use of `just`

In this chapter we'll use sample flask project to build and practice use cases around the `just` tool.

We start by going over the application and building strategy to working on it. The tree structure of it looks something like this:

```sh
| -- app
|     |-- app.py
|     |-- config.py
|     |-- requirements.txt
|     \-- tests.py

# explanations below
```
- `app.py` : main web application that does mainly nothing
- `config.py` : configuration for application, mainly implemented on python flask library based
- `tests.py` : tests written for checking the application execution
- `requirements.txt` : list of libraries needed to run the application, need to be install


---
# Strategy

Each application uses combination of running folder, running environment and libraries to provide the service of itself.
Our app is no different, using current running folder of `just-workshop/02_basic_features/app`, python environment and list of libraries in `requirements.txt` file.
To make it work in running environment we need strategy by which we can automate its start functioning which also can be called as `deployment`.
Usually deploying python/flask apps, require list of steps:
    - Setup special environment which we call `development environment`
    - Install list of libraries in `requirements.txt` to `development environment`
    - Check the syntax of code in application files
    - Test the functioning application
    - Run the application for development purposes
    - Clean `development environment`

Now usually these step can be automated with bunch of tools, such as shell scripts, ansible playbooks, Jenkins pipelines and so on.
Yet this work shop focuses on `just` thus, what we'll use it to work on our strategy with it.

---

# Initial `Justfile`

We'll create `justfile` for our purpose and start editing with strategy steps explaining each step slowly.
```sh
touch Justfile # we can create just file with any sting 
               # configuration such as justfile JustFile justFile and so on
```

---
# Adding strategy

No we can take all the steps described before hand and add them to `Justfile`
```make
# default behavior
default: setup_virt_env install_dependencies run_app

# Set development environment
setup_virt_env:
    pipenv shell
    pip install --upgrade pip wheel pip-tools

# Install dependency libraries in development environment
install_dependencies:
    python3 -m pip install -r requirements.txt

# Upgrade dependency libraries development environment
upgrade_dependencies:
    python3 -m pip install --upgrade -r requirements.txt

# Checking code syntax
lint_code:
    pylint *.py

# Testing functioning application
test_app:
    pytest tests.py
    
# Running Development Environment
run_app:
    gunicorn --reload --workers 3 --bind 0.0.0.0:8000 app:app
```

---

# Thats all folks

### Thanks for coming 

... joking

---
# `Just` help tools

`Just` has all the perks, that other tool have:

- Easy install, that we already covered at the beginning
- Editors suppose: vscode, jetbrains, vim and many others
- Backwards compatibility
- Programmability

Thus lets cover the last one in depth

---

# The Default Recipe

When just is invoked without a recipe, it runs the first recipe in the justfile.
Thus if we run `just` with no parameters, only default will run. In our example:

```make
# default behavior
default: setup_virt_env install_dependencies run_app
```
Where the default `recipe` run multiple recipes named `setup_virt_env` `install_dependencies` `run_app` that we created.

In case nothing happens, you can try listing the recipes with `just --list`

---

# Practice

- List all the recipes we have inserted into `justfile`
- Run default recipe
- Can someone explain the error ?

---

# Aliases

Aliases allow recipes to be invoked with alternative names

```make
alias l := lint_code

# Checking code syntax
lint_code:
    pylint *.py
```
and we no can invoke the recipe with  `l` argument

```sh
just l
```

---

# Practice

- Add to each recipe initial that will enable us to run it shortly
- Run lint_code , test_app and app_run with aliases only

---

# Dotenv Integration

If dotenv-load is set, just will load environment variables from a file named .env. This file can be located in the same directory as your justfile or in a parent directory. These variables are environment variables, not just variables, and so must be accessed using $VARIABLE_NAME in recipes and backticks.

For example, if your .env file contains:
```sh
# a comment, will be ignored
DATABASE_ADDRESS=localhost:6379
SERVER_PORT=1337
```
And your `justfile` contains:

set dotenv-load
```make
serve:
  @echo "Starting server with database $DATABASE_ADDRESS on port $SERVER_PORT…"
  ./server --database $DATABASE_ADDRESS --port $SERVER_PORT
```
just serve will output:
```sh
$ just serve
Starting server with database localhost:6379 on port 1337…
./server --database $DATABASE_ADDRESS --port $SERVER_PORT
```

---

# Practice

- Add .env file to out `justfile` and  insert to it PORT=8000
- Add `@echo` command to print message and use `run_app` recipe with the provided port