

---
# Installation

### Prerequisites

- `just` should run on any system with a reasonable shell, `sh`, `bash` or `zsh`, being either Linux, MacOS, or the BSDs
- On Windows, it can be enabled with the shell provided by `Git for Windows`, `GitHub Desktop`, or `Cygwin`.
    - If you’d rather not install any shell mentioned above, you can use the shell setting to use the shell of your choice. Like PowerShell or CMD
    - But we're not gonna touch that  subject here, more then mentioning

---
# Installation  

### Packages

- `Just` is develop with `rust` programming language, thus it can be install with `cargo` package manager
    - `cargo install just`
- On MacOS, you can use `brew` to install `just`:
    - `brew install just`

---
# Installation (cont.)  

### Packages
- On Linux:
    - In RedHat and Arch based distributions, default package managers should enable installation:
          - `dnf install -y just`
          - `pacman -S just`
    - In Debian based distribution, you can either build the debian package or add MPR repository:
```sh
curl -q 'https://proget.makedeb.org/debian-feeds/prebuilt-mpr.pub' | gpg --dearmor | sudo tee /usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg 1> /dev/null
echo "deb [signed-by=/usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg] https://proget.makedeb.org prebuilt-mpr $(lsb_release -cs)" | sudo tee /etc/apt/sources.list.d/prebuilt-mpr.list
sudo apt update
sudo apt install -y just
```

---

# Installation

### Binary install with script 

You can use the following command on Linux, MacOS, or Windows to download the latest release

```sh
curl --proto '=https' --tlsv1.2 -sSf https://just.systems/install.sh | bash -s -- --to /usr/local/bin
```

---

# Testing install

No matter, which installation method you have used, the most easy way to test if it works, is to print its `version`

```sh
$ just --version
    just 1.9.0
```

To expand our test, we can create a file named `justfile` in the root of your project with the following contents:
```make
recipe-name:
  echo 'This is a recipe!'

# this is a comment
another-recipe:
  @echo 'This is another recipe.'
```

---
# Testing install (cont.)

When you invoke just it looks for file `justfile` in the current directory and upwards, so you can invoke it from any subdirectory of your project.

The search for a `justfile` is case insensitive, so any case, like `Justfile`, `JUSTFILE`, or `JuStFiLe`, will work. just will also look for files with the name .`justfile`, in case you’d like to hide a `justfile`.

Running just with no arguments runs the first recipe in the justfile:
```sh
$ just
echo 'This is a recipe!'
This is a recipe!
```

One or more arguments specify the recipe(s) to run:

```sh
$ just another-recipe
This is another recipe.
```

---

# Practice

- Install just
- Create `justfile` with recipes:
  - `lint` : prints linting and wait for 2 seconds
  - `build`: prints Building and wait for 2 seconds
  - `test` : prints Testing and wait for 2 seconds
- Run `just` to check what happens
- Add `default` stage, that runs `just --list`
- Change default to run all the other recipes

> Note: in case you create several `justifies` in same folder use `-f` and file name to point `just` which one to use

