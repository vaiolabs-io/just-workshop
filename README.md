# `Just` Workshop

# About The Workshop Itself ?

We'll learn several topics mainly focused on:

- What is task runners ?
- What is Just?
- How Just works ?
- Where can we use just ?


### Who Is This workshop for ?

- Junior/senior developers who wish to automate their development tasks.
- Junior/senior Sysadmins/SysOps who wish to automate task
- Developers that use CI/CD and want to make them easier.
- Experienced developers/devops who wish to learn new skill.

---

# Workshop Topics

- Intro
- Just setup
- Basic Features
- Just Examples:
  - Python build
  - C build

# Prerequisites

- Knowledge of Nix based systems (Linux/Unix/BSD/MacOS)
- Understanding of CI/CD concepts
- Grasp of programming language
- Automation know-how

<!-- ### What is `just`?
- `just` is a command runner 
- `just` is a handy way to save and run project-specific commands.


### Cheat file link

[here](https://cheatography.com/linux-china/cheat-sheets/justfile/) -->